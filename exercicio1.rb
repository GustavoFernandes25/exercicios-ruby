=begin
    Fazer uma função que dado um Array de Arrays faça a soma e a multiplicação dele
    Ex:
    [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
    Soma:39
    Mult:100800    
=end

def somando_valor(list)# Função que faz a soma dos valores contidos no array
    soma = 0
    list.each do |num|#Entra na primeira posição do array "pai"
        for valor in num # coloca o valor da primeira posição array "filho" e soma como proximo até não ter mais.
            soma += valor# realiza a soma dos valores.
        end
    end
    return soma#retorna a soma completa de todos os valores.
end

def mult_valor (list) #Função que multiplica valores de um array
    mult = 1
    list.each do |num| # entra no primeiro valor do array "pai"
        for valor in num # entra no pimeiro valor do array "filho"
            mult *= valor # faz a multiplicação
        end
    end
    return mult # retorna o valor da multiplicação
end

#=================== Inicio do programa ===================

lista_arrays = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
valorSoma = somando_valor(lista_arrays) #chama a função  de soma e coloca o resultado na variável
valorMult = mult_valor(lista_arrays)# chama a função de multplicação e coloca o resultado na variável

print "Soma de todos os números: ", valorSoma
print "\nMultiplicação dos valores: ", valorMult, "\n"


